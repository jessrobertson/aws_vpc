## TERRAFORMING AWS VPC
# author: Jess Robertson, CSIRO Minerals
# date:   August 2018
#
# description: Terraform config to set up AWS Virtual Private Cloud for deploying
#              our compute environment into. Also creates security groups for instances.

# We're going to set up a VPC with both public and private subnets
resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = "${var.tags}"
}

# Set up the private and public subnets for two availability zones
resource "aws_subnet" "public-a" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "10.0.1.0/24"
  availability_zone = "${var.region}a"
  map_public_ip_on_launch = true
  tags = "${var.tags}"
}
resource "aws_subnet" "private-a" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "10.0.2.0/24"
  availability_zone = "${var.region}a"
  map_public_ip_on_launch = true
  tags = "${var.tags}"
}
resource "aws_subnet" "public-b" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "10.0.3.0/24"
  availability_zone = "${var.region}b"
  map_public_ip_on_launch = true
  tags = "${var.tags}"
}
resource "aws_subnet" "private-b" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "10.0.4.0/24"
  availability_zone = "${var.region}b"
  map_public_ip_on_launch = true
  tags = "${var.tags}"
}

# Now we need to define an internet gateway so that the public subnet is
# addressable from the internet
resource "aws_internet_gateway" "gateway" {
  vpc_id = "${aws_vpc.vpc.id}"
  tags = "${var.tags}"
}

# Add a route table to pass through NAT to public subnet
resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gateway.id}"
  }
  tags = "${var.tags}"
}
resource "aws_route_table_association" "public_route_table_a" {
  subnet_id = "${aws_subnet.public-a.id}"
  route_table_id = "${aws_route_table.public.id}"
}
resource "aws_route_table_association" "public_route_table_b" {
  subnet_id = "${aws_subnet.public-b.id}"
  route_table_id = "${aws_route_table.public.id}"
}

# Create security groups for public and private subnets
resource "aws_security_group" "public" {
  name = "${var.name}-public-sg"
  description = "Allow incoming HTTP(S) connections and SSH access"
  vpc_id = "${aws_vpc.vpc.id}"

  # Allow HTTP/s ingress
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks =  ["0.0.0.0/0"]
  }

  # Allow all outbound traffic to the internet
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_security_group" "private" {
  name = "${var.name}-private-sg"
  description = "Allow ingress and egress only from VPC"
  vpc_id = "${aws_vpc.vpc.id}"

  # Access only from VPC
  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "${aws_subnet.public-a.cidr_block}",
      "${aws_subnet.public-b.cidr_block}",
      "${aws_subnet.private-a.cidr_block}",
      "${aws_subnet.private-b.cidr_block}"
    ]
  }

  # Allow SSH ingress from anywhere
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all outbound traffic to VPC only
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "${aws_subnet.public-a.cidr_block}",
      "${aws_subnet.public-b.cidr_block}",
      "${aws_subnet.private-a.cidr_block}",
      "${aws_subnet.private-b.cidr_block}"
    ]
  }
}