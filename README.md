# AWS VPC - Terraform module

Manages a simple AWS VPC with two private and two public subnets, and a route table to the public internet via an internet gateway. This has been primarily set up for deploying AWS Batch jobs.

Also creates security groups for deploying instances (should maybe look at replacing with instance roles at some stage).

Usage:

```hcl
module "vpc" {
  source = "../modules/vpc"
  region = "${var.region}"
  name = "${module.labels.id}"
  tags = "${module.labels.tags}"
}
```

and then to deploy a compute environment for Batch onto the public subnets, do

```hcl
resource "aws_batch_compute_environment" "batch_compute" {
  ...
  compute_resources = {
    ...
    security_group_ids = ["${module.vpc.public_security_group}"]
    subnets = ["${module.vpc.public_subnets}"]
    ...
  }
  ...
}
```