variable "name" {
  type = "string"
  description = "A name the network infrastructure"
  default = "terraform-vpc"
}

variable "region" {
  type = "string"
  description = "The AWS region to deploy the VPC into"
}

variable "tags" {
  type = "map"
  description = "Tags for tagging the network infrastructure"
  default = {
    Name = "terraform-vpc"
  }
}