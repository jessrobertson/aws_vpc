# Main VPC
output "id" {
  value = "${aws_vpc.vpc.id}"
}

# Subnets
output "public_subnets" {
  value = [
    "${aws_subnet.public-a.id}",
    "${aws_subnet.public-b.id}"
  ]
}
output "private_subnets" {
  value = [
    "${aws_subnet.private-a.id}",
    "${aws_subnet.private-b.id}"
  ]
}

# Internet gateway
output "gateway" {
  value = "${aws_internet_gateway.gateway.id}"
}

# Security groups
output "public_security_group" {
  value = "${aws_security_group.public.id}"
}
output "private_security_group" {
  value = "${aws_security_group.private.id}"
}